package iowrapper

import (
	"bytes"
	"io"
	"io/ioutil"
	"strings"
	"testing"

	"github.com/leanovate/gopter"
	"github.com/leanovate/gopter/gen"
	"github.com/leanovate/gopter/prop"
	"github.com/stretchr/testify/assert"
)

func TestNewSeeker(t *testing.T) {
	type precondition struct {
		beforeRead int64
	}
	type args struct {
		offset int64
		whence int
	}
	type readArgs struct {
		size int64
	}
	type result struct {
		content string
		eof     bool
	}
	tests := []struct {
		name         string
		precondition precondition
		args         args
		readArgs     readArgs
		result       result
	}{
		{
			name: "read without buffer (1)",
			precondition: precondition{
				beforeRead: 0,
			},
			args: args{
				offset: 0,
				whence: io.SeekStart,
			},
			readArgs: readArgs{
				size: 5,
			},
			result: result{
				content: "01234",
			},
		},
		{
			name: "read without buffer (2)",
			precondition: precondition{
				beforeRead: 0,
			},
			args: args{
				offset: 0,
				whence: io.SeekCurrent,
			},
			readArgs: readArgs{
				size: 5,
			},
			result: result{
				content: "01234",
			},
		},
		{
			name: "read within buffer (offset 0) (1)",
			precondition: precondition{
				beforeRead: 5,
			},
			args: args{
				offset: 0,
				whence: io.SeekStart,
			},
			readArgs: readArgs{
				size: 5,
			},
			result: result{
				content: "01234",
			},
		},
		{
			name: "read within buffer (offset 0) (2)",
			precondition: precondition{
				beforeRead: 5,
			},
			args: args{
				offset: -5,
				whence: io.SeekCurrent,
			},
			readArgs: readArgs{
				size: 5,
			},
			result: result{
				content: "01234",
			},
		},
		{
			name: "read within buffer (offset not 0) (1)",
			precondition: precondition{
				beforeRead: 5,
			},
			args: args{
				offset: 2,
				whence: io.SeekStart,
			},
			readArgs: readArgs{
				size: 3,
			},
			result: result{
				content: "234",
			},
		},
		{
			name: "read within buffer (offset not 0) (2)",
			precondition: precondition{
				beforeRead: 5,
			},
			args: args{
				offset: -3,
				whence: io.SeekCurrent,
			},
			readArgs: readArgs{
				size: 3,
			},
			result: result{
				content: "234",
			},
		},
		{
			name: "read from buffer and reader (offset 0) (1)",
			precondition: precondition{
				beforeRead: 5,
			},
			args: args{
				offset: 0,
				whence: io.SeekStart,
			},
			readArgs: readArgs{
				size: 7,
			},
			result: result{
				content: "0123456",
			},
		},
		{
			name: "read from buffer and reader (offset 0) (2)",
			precondition: precondition{
				beforeRead: 5,
			},
			args: args{
				offset: -5,
				whence: io.SeekCurrent,
			},
			readArgs: readArgs{
				size: 7,
			},
			result: result{
				content: "0123456",
			},
		},
		{
			name: "read from buffer and reader (offset 0 > 0) (1)",
			precondition: precondition{
				beforeRead: 5,
			},
			args: args{
				offset: 2,
				whence: io.SeekStart,
			},
			readArgs: readArgs{
				size: 5,
			},
			result: result{
				content: "23456",
			},
		},
		{
			name: "read from buffer and reader (offset 0 > 0) (2)",
			precondition: precondition{
				beforeRead: 5,
			},
			args: args{
				offset: -3,
				whence: io.SeekCurrent,
			},
			readArgs: readArgs{
				size: 5,
			},
			result: result{
				content: "23456",
			},
		},
		{
			name: "skip read until offset and read within buffer (1)",
			precondition: precondition{
				beforeRead: 0,
			},
			args: args{
				offset: 2,
				whence: io.SeekStart,
			},
			readArgs: readArgs{
				size: 5,
			},
			result: result{
				content: "23456",
			},
		},
		{
			name: "skip read until offset and read within buffer (2)",
			precondition: precondition{
				beforeRead: 0,
			},
			args: args{
				offset: 2,
				whence: io.SeekCurrent,
			},
			readArgs: readArgs{
				size: 5,
			},
			result: result{
				content: "23456",
			},
		},
		{
			name: "offset is out of range (completely)",
			precondition: precondition{
				beforeRead: 0,
			},
			args: args{
				offset: 15,
				whence: io.SeekCurrent,
			},
			readArgs: readArgs{
				size: 5,
			},
			result: result{
				content: "",
				eof:     true,
			},
		},
		{
			name: "offset is out of range (partial)",
			precondition: precondition{
				beforeRead: 0,
			},
			args: args{
				offset: 8,
				whence: io.SeekCurrent,
			},
			readArgs: readArgs{
				size: 5,
			},
			result: result{
				content: "89",
				eof:     true,
			},
		},
		{
			name: "offset from end (offset 0, completed)",
			precondition: precondition{
				beforeRead: 15,
			},
			args: args{
				offset: -10,
				whence: io.SeekEnd,
			},
			readArgs: readArgs{
				size: 5,
			},
			result: result{
				content: "01234",
			},
		},
		{
			name: "offset from end (offset 0, not completed yet)",
			precondition: precondition{
				beforeRead: 0,
			},
			args: args{
				offset: -10,
				whence: io.SeekEnd,
			},
			readArgs: readArgs{
				size: 5,
			},
			result: result{
				content: "01234",
			},
		},
	}
	for _, tc := range tests {
		t.Run(tc.name, func(t *testing.T) {
			reader := strings.NewReader("0123456789")
			readSeeker := NewSeeker(reader)
			// fill buffer
			io.CopyN(ioutil.Discard, readSeeker, tc.precondition.beforeRead)

			_, err := readSeeker.Seek(tc.args.offset, tc.args.whence)
			assert.NoError(t, err)
			var buffer bytes.Buffer
			_, err = io.CopyN(&buffer, readSeeker, tc.readArgs.size)
			assert.Equal(t, tc.result.content, buffer.String())
			if tc.result.eof {
				assert.Equal(t, io.EOF, err)
			} else {
				assert.NoError(t, err)
			}
		})
	}
}

func TestNewSeekerError(t *testing.T) {
	type precondition struct {
		beforeRead int64
	}
	type args struct {
		offset int64
		whence int
	}
	type readArgs struct {
		size int64
	}
	type result struct {
		seekErr error
		readErr error
	}
	tests := []struct {
		name         string
		precondition precondition
		args         args
		readArgs     readArgs
		result       result
	}{
		{
			name: "seek before start (1)",
			precondition: precondition{
				beforeRead: 0,
			},
			args: args{
				offset: -1,
				whence: io.SeekStart,
			},
			result: result{
				seekErr: ErrNegativePosition,
			},
		},
		{
			name: "seek before start (2)",
			precondition: precondition{
				beforeRead: 0,
			},
			args: args{
				offset: -1,
				whence: io.SeekCurrent,
			},
			result: result{
				seekErr: ErrNegativePosition,
			},
		},
		{
			name: "seek before start (3)",
			precondition: precondition{
				beforeRead: 0,
			},
			args: args{
				offset: -20,
				whence: io.SeekEnd,
			},
			result: result{
				seekErr: ErrNegativePosition,
			},
		},
		{
			name: "invalid whence",
			precondition: precondition{
				beforeRead: 0,
			},
			args: args{
				whence: -1,
			},
			result: result{
				seekErr: ErrInvalidWhence,
			},
		},
	}
	for _, tc := range tests {
		t.Run(tc.name, func(t *testing.T) {
			reader := strings.NewReader("0123456789")
			readSeeker := NewSeeker(reader)
			// fill buffer
			io.CopyN(ioutil.Discard, readSeeker, tc.precondition.beforeRead)

			_, err := readSeeker.Seek(tc.args.offset, tc.args.whence)
			assert.Equal(t, tc.result.seekErr, err)
			if err != nil {
				return
			}
			var buffer bytes.Buffer
			_, err = io.CopyN(&buffer, readSeeker, tc.readArgs.size)
			assert.Equal(t, tc.result.readErr, err)
		})
	}
}

func TestEOF_SeekOutOfReader(t *testing.T) {
	reader := strings.NewReader("0123456789")
	readSeeker := NewSeeker(reader)
	readSeeker.Seek(15, io.SeekStart)
	_, err := io.CopyN(ioutil.Discard, readSeeker, 1)
	assert.Equal(t, io.EOF, err)
}

func TestEOF_ReadTwice(t *testing.T) {
	reader := strings.NewReader("0123456789")
	readSeeker := NewSeeker(reader)
	size, err := io.CopyN(ioutil.Discard, readSeeker, 10)
	assert.Equal(t, int64(10), size)
	assert.NotEqual(t, io.EOF, err)
	_, err = io.CopyN(ioutil.Discard, readSeeker, 10)
	assert.Equal(t, io.EOF, err)
}

func TestEOF_ReadTwiceByOversizeRead(t *testing.T) {
	reader := strings.NewReader("0123456789")
	readSeeker := NewSeeker(reader)
	size, err := io.CopyN(ioutil.Discard, readSeeker, 15)
	assert.Equal(t, int64(10), size)
	assert.Equal(t, io.EOF, err)
}

func TestEOF_EOFThenSeek(t *testing.T) {
	reader := strings.NewReader("0123456789")
	readSeeker := NewSeeker(reader)
	io.CopyN(ioutil.Discard, readSeeker, 15) // EOF
	readSeeker.Seek(0, io.SeekStart)         // reset

	size, err := io.CopyN(ioutil.Discard, readSeeker, 10)
	assert.Equal(t, int64(10), size)
	assert.NotEqual(t, io.EOF, err)
	_, err = io.CopyN(ioutil.Discard, readSeeker, 10)
	assert.Equal(t, io.EOF, err)
}

func TestBufferLimit(t *testing.T) {
	type precondition struct {
		beforeRead int64
	}
	type args struct {
		offset int64
		whence int
	}
	type readArgs struct {
		size int64
	}
	type result struct {
		seekErr error
		readErr error
	}
	tests := []struct {
		name         string
		precondition precondition
		args         args
		readArgs     readArgs
		result       result
	}{
		{
			name: "inside limit",
			precondition: precondition{
				beforeRead: 0,
			},
			args: args{
				offset: 0,
				whence: io.SeekStart,
			},
			readArgs: readArgs{
				size: 6,
			},
			result: result{
				seekErr: nil,
				readErr: nil,
			},
		},
		{
			name: "as same as limit",
			precondition: precondition{
				beforeRead: 0,
			},
			args: args{
				offset: 0,
				whence: io.SeekStart,
			},
			readArgs: readArgs{
				size: 8,
			},
			result: result{
				seekErr: nil,
				readErr: nil,
			},
		},
		{
			name: "exceed limit",
			precondition: precondition{
				beforeRead: 0,
			},
			args: args{
				offset: 0,
				whence: io.SeekStart,
			},
			readArgs: readArgs{
				size: 10,
			},
			result: result{
				seekErr: nil,
				readErr: ErrMaxSizeExceeded,
			},
		},
		{
			name: "exceed location when seeking from start",
			precondition: precondition{
				beforeRead: 0,
			},
			args: args{
				offset: 10,
				whence: io.SeekStart,
			},
			readArgs: readArgs{
				size: 6,
			},
			result: result{
				seekErr: nil,
				readErr: ErrMaxSizeExceeded,
			},
		},
		{
			name: "exceed location when seeking from end",
			precondition: precondition{
				beforeRead: 0,
			},
			args: args{
				offset: -6,
				whence: io.SeekEnd,
			},
			readArgs: readArgs{
				size: 6,
			},
			result: result{
				seekErr: nil,
				readErr: ErrMaxSizeExceeded,
			},
		},
	}
	for _, tc := range tests {
		t.Run(tc.name, func(t *testing.T) {
			reader := strings.NewReader("0123456789")
			readSeeker := NewSeeker(reader, MaxBufferSize(8))
			// fill buffer
			io.CopyN(ioutil.Discard, readSeeker, tc.precondition.beforeRead)

			_, err := readSeeker.Seek(tc.args.offset, tc.args.whence)
			assert.Equal(t, tc.result.seekErr, err)
			if err != nil {
				return
			}
			var buffer bytes.Buffer
			_, err = io.CopyN(&buffer, readSeeker, tc.readArgs.size)
			assert.Equal(t, tc.result.readErr, err)
		})
	}
}

func TestSuccessfullyRead(t *testing.T) {
	properties := gopter.NewProperties(nil)

	skipBytesGen := gen.AnyString()
	readBytesGen := gen.AnyString()
	remainedBytesGen := gen.AnyString()

	properties.Property("Read successfully with SeekStart", prop.ForAll(func(skipBytesSrc, readBytesSrc, remainedBytesSrc string) bool {
		skipBytes := []byte(skipBytesSrc)
		readBytes := []byte(readBytesSrc)
		remainedBytes := []byte(remainedBytesSrc)
		var buffer bytes.Buffer
		buffer.Write(skipBytes)
		buffer.Write(readBytes)
		buffer.Write(remainedBytes)

		readSeeker := NewSeeker(&buffer)
		readSeeker.Seek(int64(len(skipBytes)), io.SeekStart)

		var output bytes.Buffer
		n, err := io.CopyN(&output, readSeeker, int64(len(readBytes)))
		if n != int64(len(readBytes)) {
			t.Logf("Read successfully with SeekStart fail: n(%d) != len(readBytes)()%d\n", n, len(readBytes))
			return false
		}
		if output.String() != readBytesSrc {
			t.Logf("Read successfully with SeekStart fail: %s != %s\n", output.String(), readBytesSrc)
			return false
		}
		if err != nil {
			t.Logf("Read successfully with SeekStart fail: err(%v) != nil\n", err)
			return false
		}
		return true
	}, skipBytesGen, readBytesGen, remainedBytesGen))

	properties.Property("Read successfully with SeekCurrent", prop.ForAll(func(skipBytesSrc, readBytesSrc, remainedBytesSrc string) bool {
		skipBytes := []byte(skipBytesSrc)
		readBytes := []byte(readBytesSrc)
		remainedBytes := []byte(remainedBytesSrc)
		var buffer bytes.Buffer
		buffer.Write(skipBytes)
		buffer.Write(readBytes)
		buffer.Write(remainedBytes)

		readSeeker := NewSeeker(&buffer)
		readSeeker.Seek(int64(len(skipBytes)), io.SeekCurrent)

		var output bytes.Buffer
		n, err := io.CopyN(&output, readSeeker, int64(len(readBytes)))
		if n != int64(len(readBytes)) {
			t.Logf("Read successfully with SeekStart fail: n(%d) != len(readBytes)()%d\n", n, len(readBytes))
			return false
		}
		if output.String() != readBytesSrc {
			t.Logf("Read successfully with SeekStart fail: %s != %s\n", output.String(), readBytesSrc)
			return false
		}
		if err != nil {
			t.Logf("Read successfully with SeekStart fail: err(%v) != nil\n", err)
			return false
		}
		return true
	}, skipBytesGen, readBytesGen, remainedBytesGen))

	properties.Property("Read successfully with SeekEnd", prop.ForAll(func(skipBytesSrc, readBytesSrc, remainedBytesSrc string) bool {
		skipBytes := []byte(skipBytesSrc)
		readBytes := []byte(readBytesSrc)
		remainedBytes := []byte(remainedBytesSrc)
		var buffer bytes.Buffer
		buffer.Write(skipBytes)
		buffer.Write(readBytes)
		buffer.Write(remainedBytes)

		readSeeker := NewSeeker(&buffer)
		readSeeker.Seek(-int64(len(remainedBytes) + len(readBytes)), io.SeekEnd)

		var output bytes.Buffer
		n, err := io.CopyN(&output, readSeeker, int64(len(readBytes)))
		if n != int64(len(readBytes)) {
			t.Logf("Read successfully with SeekStart fail: n(%d) != len(readBytes)()%d\n", n, len(readBytes))
			return false
		}
		if output.String() != readBytesSrc {
			t.Logf("Read successfully with SeekStart fail: %s != %s\n", output.String(), readBytesSrc)
			return false
		}
		if err != nil {
			t.Logf("Read successfully with SeekStart fail: err(%v) != nil\n", err)
			return false
		}
		return true
	}, skipBytesGen, readBytesGen, remainedBytesGen))

	properties.TestingRun(t)
}

func TestFuzzing(t *testing.T) {
	properties := gopter.NewProperties(nil)

	srcGen := gen.AnyString()
	whenceGen := gen.IntRange(0, 2)
	offsetGen := gen.Int64()
	readBytesGen := gen.Int64()

	properties.Property("Don't panic", prop.ForAll(func(srcStr string, whence int, offset, readBytes int64) bool {
		src := strings.NewReader(srcStr)
		readSeeker := NewSeeker(src)
		readSeeker.Seek(offset, whence)
		io.CopyN(ioutil.Discard, readSeeker, readBytes)
		return true
	}, srcGen, whenceGen, offsetGen, readBytesGen))

	properties.TestingRun(t)
}
