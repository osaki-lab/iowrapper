# iowrapper

[![Go Reference](https://pkg.go.dev/badge/gitlab.com/osaki-lab/iowrapper.svg)](https://pkg.go.dev/gitlab.com/osaki-lab/iowrapper)

This package provides io package helper. 

Now it provides only wrapper to generate io.ReadSeeker from io.Reader.
Some API requires io.ReadSeeker instead of io.Reader.
This package keeps the content in buffer and behaves as io.SeekReader.

## Example

```go
package webhandler
import "io/ioutil"

func upload(w http.ResponseWriter, r *http.Request) {
    sess, err := session.NewSession(&aws.Config{Region: aws.String("us-east-1")})
    if err != nil {
        http.Error(w, "session error", 500)
    }
    defer r.Body.Close()
    svc := s3manager.NewUploader(sess)
    input := &s3manager.UploadInput{
        Bucket: aws.String("bucket"),
        Key: aws.String("whatever"),
        Body: iowrapper.NewSeeker(r.Body), // here
    }
    _, err = svc.Upload(input)
    if err != nil {
        http.Error(w, "upload error", 500)
    }
    io.Copy(ioutil.Discard, r.Body)
    io.WriteString(w, "upload success")
}
```

## Install

```sh
go get -u gitlab.com/osaki-lab/iowrapper
```

## Reference

This package provides only one function.

### ``iowrapper.NewSeeker(r io.Reader, options ...Option) io.ReadSeeker``

This function wraps io.Reader and return io.ReadSeeker.

Default buffer size to keep upstream reader's content is 1MB.
To keep away from security issue, this library decides max memory usage.

You can change buffer size by using the following function:

* ``DefaultMaxBufferSize(maxSize int64)``

```go
readSeeker := iowrapper.NewSeeker(reader,
    iowrapper.DefaultMaxBufferSize(100 * 1024 * 1024)) // 100MB
```

## License

Apache License Version 2.0